import React, { useState } from "react";
import "./App.css";

const FeedBack = () => <h1>give feedback</h1>;

const Button = ({ onClick, text }) => <button onClick={onClick}>{text}</button>;

const StatisticsText = ({ title, text, value }) => {
  return (
    <>
      <h1>{title}</h1>
      <tr>
        <td>{text} </td>
        <td>{value}</td> 
      </tr>
    </>
  );
};

const Statistics = ({ good, neutral, bad, all, average, positive }) => {
  if (all === 0) {
    return (
      <>
        <h1>Statistic</h1>
        <p>No feedback given</p>
      </>
    )
    
  } else {
    return (
      <>
        <StatisticsText title="Statistic" />
        <StatisticsText text="good" value={good} />
        <StatisticsText text="neutral" value={neutral} />
        <StatisticsText text="bad" value={bad} />
        <StatisticsText text="all" value={all} />
        <StatisticsText text="average" value={average} />
        <StatisticsText text="positive" value={positive} />
      </>
    );
  }
};

function App() {
  const [good, setGood] = useState(0);
  const [neutral, setNeutral] = useState(0);
  const [bad, setBad] = useState(0);
  const [all, setAll] = useState(0);
  const [arrayOfScore, setArrayOfScore] = useState([]);

  const incrementGood = () => {
    setGood(good + 1);
    setAll(all + 1);
    setArrayOfScore(arrayOfScore.concat(1));
  };

  const incrementNeutral = () => {
    setNeutral(neutral + 1);
    setAll(all + 1);
    setArrayOfScore(arrayOfScore.concat(0));
  };

  const incrementBad = () => {
    setBad(bad + 1);
    setAll(all + 1);
    setArrayOfScore(arrayOfScore.concat(-1));
  };

  const calculateAverage = () => {
    let sum = 0;
    if (arrayOfScore.length > 0) {
      for (let number of arrayOfScore) {
        sum += number;
      }
      const total = sum / arrayOfScore.length;

      return total;
    }
  };

  const calculatePositive = () => {
    if (all > 0) {
      const total = (good / all) * 100;
      return total;
    } else {
      return 0;
    }
  };

  return (
    <div>
      <FeedBack />
      <Button onClick={incrementGood} text={"good"} />
      <Button onClick={incrementNeutral} text={"neutral"} />
      <Button onClick={incrementBad} text={"bad"} />
      <Statistics
        title="Statistic"
        good={good}
        neutral={neutral}
        bad={bad}
        all={all}
        average={calculateAverage()}
        positive={calculatePositive()}
      />
    </div>
  );
}

export default App;
